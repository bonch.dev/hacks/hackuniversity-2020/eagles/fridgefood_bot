package product_search

import (
    "context"
    "database/sql"
    "sort"
    "strings"

    "github.com/Prinzhorn/nicenshtein"
    "github.com/lithammer/fuzzysearch/fuzzy"
    log "github.com/sirupsen/logrus"
    "gitlab.com/bonch.dev/hacks/hackuniversity-2020/eagles/fridgefood_bot/models"
)

func NewProductSearch(db *sql.DB, logger *log.Logger) *ProductSearch {
    slice, err := models.Products().All(context.Background(), db); if err != nil {
        logger.Fatal(err)
    }

    mappedProducts := make(map[string]*models.Product, 0)
    ns := nicenshtein.NewNicenshtein()
    var strings []string


    for _, el := range slice {
        strings = append(strings, el.Name)
        mappedProducts[el.Name] = el
        ns.AddWord(el.Name)
    }

    return &ProductSearch{products: mappedProducts, ns: ns, strings: strings}
}

type ProductSearch struct {
    ns nicenshtein.Nicenshtein
    products map[string]*models.Product
    strings []string
}

func (ps *ProductSearch) Search(what string) []*models.Product {
    resultsL := make(map[string]int, 0)
    ps.ns.CollectWords(&resultsL, what, 2)

    resultsF := fuzzy.RankFindNormalizedFold(what, ps.strings)
    sort.Sort(resultsF)

    var searchResults []*models.Product

    for k := range resultsL {
        searchResults = AppendIfMissing(searchResults, ps.products[k])
    }

    for _, r := range resultsF {
        searchResults = AppendIfMissing(searchResults, ps.products[r.Target])
    }

    sort.SliceStable(searchResults, func(i, j int) bool {
        return strings.Contains(searchResults[i].Name, what)
    })

    return searchResults
}

func AppendIfMissing(slice []*models.Product, i *models.Product) []*models.Product {
    for _, ele := range slice {
        if ele == i {
            return slice
        }
    }
    return append(slice, i)
}