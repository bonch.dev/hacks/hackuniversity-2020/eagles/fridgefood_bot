package handlers

import (
	tb "gitlab.com/bonch.dev/go-lib/telebot"
	"gitlab.com/bonch.dev/hacks/hackuniversity-2020/eagles/fridgefood_bot/models"
)

func BackHandle(handler *Handler) func(m *tb.Message) {
	return (&backHandler{*handler}).HandleMessage
}

func BackCallbackHandle(handler *Handler) func(m *tb.Callback) {
	return (&backHandler{*handler}).Handle
}

type backHandler struct {
	Handler
}

func (h *backHandler) HandleMessage(m *tb.Message) {
	var user *models.User

	user, err := h.getAuthUserById(m.Sender)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	if err := h.setUserState(user, MainState); err != nil {
		if _, err := h.bot.Send(m.Sender, "Я завис, нажми на кнопку еще раз"); err != nil {
			h.logger.Errorf("[text] [user found] [send error]: %v", err)
		}
	}

	if _, err := h.bot.Send(m.Sender, mainMenu, &tb.ReplyMarkup{
		ReplyKeyboard: mainKeyboard,
	}); err != nil {
		h.logger.Errorf("[new user] [send error]: %v", err)
	}
}

func (h *backHandler) Handle(c *tb.Callback) {
	var user *models.User

	user, err := h.getAuthUserById(c.Sender)
	if err != nil {
		if _, err := h.bot.Send(c.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	if err := h.setUserState(user, MainState); err != nil {
		if _, err := h.bot.Send(c.Sender, "Я завис, нажми на кнопку еще раз"); err != nil {
			h.logger.Errorf("[text] [user found] [send error]: %v", err)
		}
	}

	if _, err := h.bot.Send(c.Sender, mainMenu, &tb.ReplyMarkup{
		ReplyKeyboard: mainKeyboard,
	}); err != nil {
		h.logger.Errorf("[new user] [send error]: %v", err)
	}

	_ = h.bot.Respond(c, &tb.CallbackResponse{ShowAlert: false})
}
