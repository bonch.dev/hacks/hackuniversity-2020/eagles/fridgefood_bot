package handlers

type TelegramState string

var (
	MainState             TelegramState = "MainState"
	AddProductStartState  TelegramState = "AddProductStartState"
	AddProductChooseState TelegramState = "AddProductChooseState"
	AddProductCountState  TelegramState = "AddProductCountState"

	CookStartState    TelegramState = "CookStartState"
	CookCategoryState TelegramState = "CookCategoryState"
)
