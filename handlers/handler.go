package handlers

import (
	"context"
	"database/sql"
	"errors"

	log "github.com/sirupsen/logrus"
	"github.com/volatiletech/sqlboiler/boil"
	tb "gitlab.com/bonch.dev/go-lib/telebot"
	"gitlab.com/bonch.dev/hacks/hackuniversity-2020/eagles/fridgefood_bot/models"
	ps "gitlab.com/bonch.dev/hacks/hackuniversity-2020/eagles/fridgefood_bot/product_search"
)

type Handler struct {
	bot    *tb.Bot
	db     *sql.DB
	logger *log.Logger
	ps     *ps.ProductSearch
}

func NewHandler(bot *tb.Bot, db *sql.DB, logger *log.Logger, ps *ps.ProductSearch) *Handler {
	return &Handler{bot: bot, db: db, logger: logger, ps: ps}
}

func (h *Handler) getAuthUserById(sender *tb.User) (*models.User, error) {
	senderId := sender.Recipient()
	authUser, err := models.Users(
		models.UserWhere.TelegramID.EQ(senderId),
	).One(context.Background(),
		h.db,
	)
	if err != nil {
		return nil, errors.New("user not found")
	}

	return authUser, nil
}

func (h *Handler) setUserState(user *models.User, state TelegramState) error {
	user.TelegramState = string(state)

	if _, err := user.Update(context.Background(), h.db, boil.Infer()); err != nil {
		h.logger.Errorf("[text] db error: %v", err)
		return err
	}

	return nil
}
