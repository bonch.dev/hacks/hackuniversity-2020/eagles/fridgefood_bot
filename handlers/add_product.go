package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/volatiletech/sqlboiler/boil"
	tb "gitlab.com/bonch.dev/go-lib/telebot"
	"gitlab.com/bonch.dev/hacks/hackuniversity-2020/eagles/fridgefood_bot/models"
)

type addProductAdditional struct {
	ProductId int `json:"product_id"`
}

func AddProductHandle(handler *Handler) func(m *tb.Message) {
	return (&addProductHandler{*handler}).Handle
}

type addProductHandler struct {
	Handler
}

func (h *addProductHandler) Handle(m *tb.Message) {
	var user *models.User

	user, err := h.getAuthUserById(m.Sender)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	if err := h.setUserState(user, AddProductStartState); err != nil {
		if _, err := h.bot.Send(m.Sender, stucked); err != nil {
			h.logger.Errorf("[text] [user found] [send error]: %v", err)
		}
	}

	if _, err := h.bot.Send(m.Sender, addProductText, &tb.ReplyMarkup{
		ReplyKeyboardRemove: true,
	}); err != nil {
		h.logger.Errorf("[text] [user found] [send error]: %v", err)
	}
}

func (h *addProductHandler) HandleText(m *tb.Message) {
	var buttons [][]tb.ReplyButton

	var raw []tb.ReplyButton
	var iter = 1

	for _, el := range h.ps.Search(m.Text) {
		raw = append(raw, tb.ReplyButton{Text: el.Name})
		if iter%2 == 0 {
			buttons = append(buttons, raw)
			raw = []tb.ReplyButton{}
		}
		if iter >= 6 {
			break
		}
		iter++
	}
	buttons = append(buttons, raw)

	user, err := h.getAuthUserById(m.Sender)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	if err := h.setUserState(user, AddProductChooseState); err != nil {
		if _, err := h.bot.Send(m.Sender, stucked); err != nil {
			h.logger.Errorf("[text] [user found] [send error]: %v", err)
		}
	}

	if _, err := h.bot.Send(m.Sender, chooseProduct, &tb.ReplyMarkup{
		ReplyKeyboard:       buttons,
		ResizeReplyKeyboard: true,
	}); err != nil {
		h.logger.Errorf("[text] [send error]: %v", err)
	}
}

func (h *addProductHandler) HandleProduct(m *tb.Message) {
	user, err := h.getAuthUserById(m.Sender)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	product, err := models.Products(
		models.ProductWhere.Name.EQ(m.Text),
	).One(context.Background(), h.db)
	if err != nil {
		h.logger.Errorf("[product not found]: %v", err)

		if _, err := h.bot.Send(m.Sender, fmt.Sprintf(productNotFound, m.Text)); err != nil {
			h.logger.Errorf("[text] [product not found] [send error]: %v", err)
		}

		return
	}

	measure, err := product.Measure().One(context.Background(), h.db)
	if err != nil {
		h.logger.Errorf("[product add] [measure] %v", err)
		if _, err := h.bot.Send(m.Sender, stucked); err != nil {
			h.logger.Errorf("[product add] [measure] [send error]: %v", err)
		}
	}

	additional, err := json.Marshal(&addProductAdditional{ProductId: product.ID})
	if err != nil {
		h.logger.Errorf("[product add] [product id save] %v", err)
		if _, err := h.bot.Send(m.Sender, stucked); err != nil {
			h.logger.Errorf("[product add] [additional] [send error]: %v", err)
		}
	}

	user.TelegramAdditional = string(additional)

	if err := h.setUserState(user, AddProductCountState); err != nil {
		if _, err := h.bot.Send(m.Sender, stucked); err != nil {
			h.logger.Errorf("[product add] [state] [send error]: %v", err)
		}
	}

	if _, err := h.bot.Send(
		m.Sender,
		fmt.Sprintf(countAdd,
			measure.Name,
		),
		&tb.ReplyMarkup{
			ReplyKeyboardRemove: true,
		}); err != nil {
		h.logger.Errorf("[text] [send error]: %v", err)
	}
}

func (h *addProductHandler) HandleCount(m *tb.Message) {
	user, err := h.getAuthUserById(m.Sender)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	additionalDataStr := user.TelegramAdditional

	var additionalData addProductAdditional

	if err := json.Unmarshal([]byte(additionalDataStr), &additionalData); err != nil {
		h.logger.Info(additionalDataStr)
		h.logger.Errorf("[product add] [product count save] %v", err)
		if _, err := h.bot.Send(m.Sender, stuckedNumber); err != nil {
			h.logger.Errorf("[product add] [additional] [send error]: %v", err)
		}

		return
	}

	count, err := strconv.ParseFloat(m.Text, 64)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, wrongNumber); err != nil {
			h.logger.Errorf("[text] [product not found] [send error]: %v", err)
		}

		return
	}

	pu, err := models.ProductUsers(
		models.ProductUserWhere.ProductID.EQ(additionalData.ProductId),
		models.ProductUserWhere.UserID.EQ(user.ID),
	).One(context.Background(), h.db)
	if err != nil {
		pu = &models.ProductUser{
			Count:     0,
			ProductID: additionalData.ProductId,
			UserID:    user.ID,
		}

		if err := pu.Insert(context.Background(), h.db, boil.Infer()); err != nil {
			h.logger.Errorf("[add product] [cant save new] %v", err)
		}
	}

	pu.Count += float32(count)

	if _, err := pu.Update(context.Background(), h.db, boil.Infer()); err != nil {
		if _, err := h.bot.Send(m.Sender, cantAdd); err != nil {
			h.logger.Errorf("[text] [product not found] [send error]: %v", err)
		}

		return
	}

	if _, err := h.bot.Send(
		m.Sender,
		fridgeUpdated,
		&tb.ReplyMarkup{
			ReplyKeyboard: mainKeyboard,
		}); err != nil {
		h.logger.Errorf("[text] [send error]: %v", err)
	}

	if err := h.setUserState(user, MainState); err != nil {
		if _, err := h.bot.Send(m.Sender, stucked); err != nil {
			h.logger.Errorf("[text] [user found] [send error]: %v", err)
		}
	}
}
