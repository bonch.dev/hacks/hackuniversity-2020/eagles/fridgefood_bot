package handlers

import (
	tb "gitlab.com/bonch.dev/go-lib/telebot"
	"gitlab.com/bonch.dev/hacks/hackuniversity-2020/eagles/fridgefood_bot/models"
)

func TextHandle(handler *Handler) func(m *tb.Message) {
	return (&textHandler{*handler}).Handle
}

type textHandler struct {
	Handler
}

func (h *textHandler) Handle(m *tb.Message) {
	var user *models.User

	user, err := h.getAuthUserById(m.Sender)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	if m.Text == back {
		(&startHandler{h.Handler}).Handle(m)
		return
	}

	switch TelegramState(user.TelegramState) {
	case AddProductStartState:
		(&addProductHandler{h.Handler}).HandleText(m)
		return
	case AddProductChooseState:
		(&addProductHandler{h.Handler}).HandleProduct(m)
		return
	case AddProductCountState:
		(&addProductHandler{h.Handler}).HandleCount(m)
		return
	case CookStartState:
		(&cookHandler{h.Handler}).HandleCategory(m)
		return
	}

	if _, err := h.bot.Send(m.Sender, needHelp); err != nil {
		h.logger.Errorf("[text] [user found] [send error]: %v", err)
	}
}
