package handlers

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries"
	"github.com/volatiletech/sqlboiler/queries/qm"
	tb "gitlab.com/bonch.dev/go-lib/telebot"
	"gitlab.com/bonch.dev/hacks/hackuniversity-2020/eagles/fridgefood_bot/models"
)

func CookHandle(handler *Handler) func(m *tb.Message) {
	return (&cookHandler{*handler}).Handle
}

func InlineCookHandle(handler *Handler) func(c *tb.Callback) {
	return (&cookHandler{*handler}).InlineCookHandle
}

type cookHandler struct {
	Handler
}

func (h *cookHandler) Handle(m *tb.Message) {
	var user *models.User

	user, err := h.getAuthUserById(m.Sender)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	categories, err := models.Categories().All(context.Background(), h.db)
	if err != nil {
		h.logger.Fatal(err)
	}

	var categoryKeyboard [][]tb.ReplyButton
	var categoryRow []tb.ReplyButton
	var iter = 1

	for _, category := range categories {
		categoryRow = append(categoryRow, tb.ReplyButton{Text: category.Name})
		if iter%2 == 0 {
			categoryKeyboard = append(categoryKeyboard, categoryRow)
			categoryRow = []tb.ReplyButton{}
		}
		if iter >= 6 {
			break
		}
		iter++
	}
	categoryKeyboard = append(categoryKeyboard, categoryRow)
	categoryKeyboard = append(categoryKeyboard, []tb.ReplyButton{Back})

	if err := h.setUserState(user, CookStartState); err != nil {
		if _, err := h.bot.Send(m.Sender, stucked); err != nil {
			h.logger.Errorf("[text] [user found] [send error]: %v", err)
		}
	}

	if _, err := h.bot.Send(m.Sender, chooseCategory, &tb.ReplyMarkup{
		ReplyKeyboard: categoryKeyboard,
	}); err != nil {
		h.logger.Errorf("[text] [user found] [send error]: %v", err)
	}
}

const randomRecipeQuery = "SELECT r.*\n" +
	"   FROM recipes r\n" +
	"   RIGHT JOIN category_recipe cr ON cr.recipe_id = r.id\n" +
	"WHERE r.id IN (SELECT recipe_id FROM product_recipe pr LEFT JOIN product_user pu ON pu.product_id = pr.product_id AND pu.user_id = %v GROUP BY pr.recipe_id HAVING (count( pr.recipe_id ) - 2) <= sum( IF ( isnull( pu.user_id ), 0, 1 ) ) )\n" +
	"AND cr.category_id = %v\n" +
	"ORDER BY RAND()\n" +
	"LIMIT 0, 1"

func (h *cookHandler) HandleCategory(m *tb.Message) {
	var user *models.User

	user, err := h.getAuthUserById(m.Sender)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	category, err := models.Categories(
		models.CategoryWhere.Name.EQ(m.Text),
	).One(context.Background(), h.db)
	if err != nil {
		h.logger.Errorf("[cook] [category load] %v", err)

		if _, err := h.bot.Send(m.Sender, "Холодильник не поддался открытию, попробуй чуть позже"); err != nil {
			h.logger.Errorf("[text] [user products] [send error]: %v", err)
		}

		return
	}

	var randomRecipe models.Recipe

	if err := queries.Raw(fmt.Sprintf(randomRecipeQuery, user.ID, category.ID)).Bind(context.Background(), h.db, &randomRecipe); err != nil {
		if err == sql.ErrNoRows {
			if _, err := h.bot.Send(m.Sender, "Время пополнить холодильник, я не могу тебе ничего предложить..."); err != nil {
				h.logger.Errorf("[text] [user products] [send error]: %v", err)
			}

			return
		}

		if _, err := h.bot.Send(m.Sender, stucked); err != nil {
			h.logger.Errorf("[text] [user found] [send error]: %v", err)
		}

		return
	}

	pr, err := randomRecipe.ProductRecipes(
		qm.Load(models.ProductRecipeRels.Product),
		qm.OrderBy("count desc"),
	).All(context.Background(), h.db)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, stucked); err != nil {
			h.logger.Errorf("[text] [user found] [send error]: %v", err)
		}
	}

	pu, err := user.ProductUsers().All(context.Background(), h.db)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, stucked); err != nil {
			h.logger.Errorf("[text] [user found] [send error]: %v", err)
		}
	}

	userProductsList := make(map[int]float32, 0)

	for _, p := range pu {
		userProductsList[p.ProductID] = p.Count
	}

	measures, err := models.Measures().All(context.Background(), h.db)
	if err != nil {
		h.logger.Errorf("[measure load] %v", err)

		if _, err := h.bot.Send(m.Sender, "Холодильник не поддался открытию, попробуй чуть позже"); err != nil {
			h.logger.Errorf("[text] [user products] [send error]: %v", err)
		}

		return
	}

	measureList := make(map[int]string, 0)

	for _, m := range measures {
		measureList[m.ID] = m.Name
	}

	products := ""
	var iter = 1
	for _, product := range pr {
		var countText string

		count, ok := userProductsList[product.ProductID]
		if !ok {
			countText = "нет этого продукта"
		} else {
			countText = h.Escape(fmt.Sprintf("%v %s", count, measureList[product.R.Product.MeasureID.Int]))
		}

		var needProductText string

		if product.Count == 0 {
			needProductText = "по вкусу"
		} else {
			needProductText = h.Escape(fmt.Sprintf(
				"%v %s",
				product.Count,
				measureList[product.R.Product.MeasureID.Int],
			))
		}

		iterText := fmt.Sprintf("%v\\. ", iter)

		var newLineSpaces string

		for i := 0; i <= len(iterText); i++ {
			newLineSpaces += " "
		}

		products += fmt.Sprintf(
			"\n%s*_%s_ %s*\n%sу тебя %s\n",
			iterText,
			product.R.Product.Name,
			needProductText,
			newLineSpaces,
			countText,
		)
		iter++
	}

	var from string
	if randomRecipe.FromURL != "" {
		from = fmt.Sprintf("[\\[Источник\\]](%s)\n", randomRecipe.FromURL)
	}

	receipt := fmt.Sprintf(
		"Твое блюдо: %s%s\nНеобходимые продукты:%s\n*Время на готовку: %s*",
		h.Escape(randomRecipe.Name),
		from,
		products,
		h.Escape(h.ParseTime(randomRecipe.CookingTime)),
	)

	var inlineCook = InlineCook

	var inlineCookData = struct {
		ReceiptId int `json:"receipt_id"`
		Price     int `json:"price"`
	}{
		ReceiptId: randomRecipe.ID,
		Price:     randomRecipe.Price,
	}

	if randomRecipe.Price > 0 {
		inlineCook.Text = fmt.Sprintf(wannaCookPaid, randomRecipe.Price)
	}

	inlineCookDataString, err := json.Marshal(inlineCookData)
	if err != nil {
		h.logger.Error(err)
	}

	inlineCook.Data = string(inlineCookDataString)

	if !randomRecipe.PhotoURL.IsZero() {
		if _, err := h.bot.Send(m.Sender, &tb.Photo{
			File:    tb.FromURL(randomRecipe.PhotoURL.String),
			Caption: receipt,
		},
			&tb.ReplyMarkup{InlineKeyboard: [][]tb.InlineButton{
				{inlineCook},
			}},
			tb.ParseMode("MarkdownV2"),
			tb.NoPreview,
		); err != nil {
			h.logger.Errorf("[text] [user products] [send error]: %v", err)
			h.logger.Error(receipt)
		}
	} else {
		if _, err := h.bot.Send(
			m.Sender,
			receipt,
			&tb.ReplyMarkup{InlineKeyboard: [][]tb.InlineButton{
				{inlineCook},
			}},
			tb.ParseMode("MarkdownV2"),
			tb.NoPreview,
		); err != nil {
			h.logger.Errorf("[text] [user products] [send error]: %v", err)
			h.logger.Error(receipt)
		}
	}
}

func (h *cookHandler) Escape(what string) string {
	what = strings.Replace(what, ".", "\\.", -1)
	what = strings.Replace(what, "(", "\\(", -1)
	what = strings.Replace(what, ")", "\\)", -1)
	what = strings.Replace(what, "-", "\\-", -1)
	what = strings.Replace(what, "+", "\\+", -1)
	what = strings.Replace(what, "!", "\\!", -1)

	return what
}

func (h *cookHandler) ParseTime(time string) string {
	var parsedTime string

	t := strings.Split(time, ":")

	if ph, err := strconv.ParseInt(t[0], 10, 64); ph > 0 && err == nil {
		parsedTime += fmt.Sprintf("%d ч ", ph)
	}

	if pm, err := strconv.ParseInt(t[1], 10, 64); pm > 0 && err == nil {
		parsedTime += fmt.Sprintf("%d мин", pm)
	}

	return parsedTime
}

func (h *cookHandler) InlineCookHandle(c *tb.Callback) {
	_ = h.bot.Respond(c)

	var user *models.User

	user, err := h.getAuthUserById(c.Sender)
	if err != nil {
		if _, err := h.bot.Send(c.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	var inlineCookData struct {
		ReceiptId int `json:"receipt_id"`
		Price     int `json:"price"`
	}

	if err := json.Unmarshal([]byte(c.Data), &inlineCookData); err != nil {
		h.logger.Error(err)

		if _, err := h.bot.Send(c.Sender, "Мы потеряли этот рецепт :(, попробуй другой"); err != nil {
			h.logger.Errorf("[text] [user products] [send error]: %v", err)
		}

		return
	}

	receipt, err := models.FindRecipe(context.Background(), h.db, inlineCookData.ReceiptId)
	if err != nil {
		if _, err := h.bot.Send(c.Sender, "Мы потеряли этот рецепт :(, попробуй другой"); err != nil {
			h.logger.Errorf("[text] [user products] [send error]: %v", err)
		}

		return
	}

	if user.Balance < inlineCookData.Price {
		if _, err := h.bot.Send(c.Sender, notEnoughBalance); err != nil {
			h.logger.Errorf("[text] [user products] [send error]: %v", err)
		}

		return
	} else {
		user.Balance -= inlineCookData.Price
		_, err := user.Update(context.Background(), h.db, boil.Infer())
		if err != nil {
			h.logger.Error(err)
		}
	}

	var receiptText = fmt.Sprintf("Твой рецепт:\n\n%s", h.Escape(receipt.Description))

	_, err = h.bot.Send(c.Sender, receiptText, tb.ParseMode("MarkdownV2"))
	if err != nil {
		h.logger.Error(err)
	}

	pr, err := receipt.ProductRecipes().All(context.Background(), h.db)
	if err != nil {
		h.logger.Error(err)
	}

	pu, err := user.ProductUsers().All(context.Background(), h.db)
	if err != nil {
		h.logger.Error(err)
	}

	userProductsList := make(map[int]*models.ProductUser, 0)

	for _, el := range pu {
		userProductsList[el.ProductID] = el
	}

	for _, el := range pr {
		up, ok := userProductsList[el.ProductID]
		if !ok {
			if err := user.AddProductUsers(context.Background(), h.db, true, &models.ProductUser{
				ProductID: el.ProductID,
				Count:     0,
			}); err != nil {
				h.logger.Error(err)
			}
		} else {
			if up.Count-el.Count < 0 {
				up.Count = 0
			} else {
				up.Count -= el.Count
			}
			_, err = up.Update(context.Background(), h.db, boil.Infer())
			if err != nil {
				h.logger.Error(err)
			}
		}
	}
}
