package handlers

import (
	"context"
	"fmt"

	"github.com/volatiletech/sqlboiler/queries/qm"
	tb "gitlab.com/bonch.dev/go-lib/telebot"
	"gitlab.com/bonch.dev/hacks/hackuniversity-2020/eagles/fridgefood_bot/models"
)

func FridgeHandle(handler *Handler) func(m *tb.Message) {
	return (&fridgeHandler{*handler}).Handle
}

type fridgeHandler struct {
	Handler
}

func (h *fridgeHandler) Handle(m *tb.Message) {
	var user *models.User

	user, err := h.getAuthUserById(m.Sender)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	userProducts, err := user.ProductUsers(
		qm.Load(models.ProductUserRels.Product),
		qm.OrderBy("count asc"),
	).All(context.Background(), h.db)
	if err != nil {
		h.logger.Errorf("[fridge load] %v", err)

		if _, err := h.bot.Send(m.Sender, "Холодильник не поддался открытию, попробуй чуть позже"); err != nil {
			h.logger.Errorf("[text] [user products] [send error]: %v", err)
		}

		return
	}

	measures, err := models.Measures().All(context.Background(), h.db)
	if err != nil {
		h.logger.Errorf("[measure load] %v", err)

		if _, err := h.bot.Send(m.Sender, "Холодильник не поддался открытию, попробуй чуть позже"); err != nil {
			h.logger.Errorf("[text] [user products] [send error]: %v", err)
		}

		return
	}

	measureList := make(map[int]string, 0)

	for _, m := range measures {
		measureList[m.ID] = m.Name
	}

	fridgeList := fridge + "\n\nВ твоем холодильнике есть:\n"

	var iter = 1
	for _, up := range userProducts {
		format := "\n%d. %s: %v %s"
		if up.Count <= 0 {
			format = "\n%d. ⚠️ %s: %v %s"
		}

		fridgeList = fridgeList + fmt.Sprintf(
			format,
			iter,
			up.R.Product.Name,
			up.Count,
			measureList[up.R.Product.MeasureID.Int],
		)
		iter++
	}

	if _, err := h.bot.Send(m.Sender, fridgeList); err != nil {
		h.logger.Errorf("[text] [user products] [send error]: %v", err)
	}
}
