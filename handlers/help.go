package handlers

import (
	tb "gitlab.com/bonch.dev/go-lib/telebot"
)

func HelpHandle(handler *Handler) func(m *tb.Message) {
	return (&helpHandler{*handler}).Handle
}

type helpHandler struct {
	Handler
}

func (h *helpHandler) Handle(m *tb.Message) {
	if _, err := h.bot.Send(m.Sender, helpText, &tb.ReplyMarkup{
		ReplyKeyboard:   mainKeyboard,
		OneTimeKeyboard: true,
	}); err != nil {
		h.logger.Errorf("[new user] [send error]: %v", err)
	}
}
