package handlers

const helpText = "text"

const addProductText = addProducts +
	"\n\nВведи название продукта, чтобы добавить его в твой \"Холодильник\"" +
	"\n\nИли нажми /back, чтобы вернуться в предыдущее меню"

const databaseError = "Произошла ошибка в работе с базой данных, попробуйте позже."
const countAdd = "Введи, сколько %v. продукта ты ложишь в холодильник"
const productNotFound = "Ты нажал на кнопку? Не могу найти %v"
const chooseCategory = cook + "\n\nВыбери категорию"
const chooseProduct = "Выбери продукт из предложенных"
const stucked = "Я завис, нажми на кнопку еще раз"
const stuckedNumber = "Я завис, введи число еще раз"
const cantAdd = "Я не смог положить в холодильник, попробуй позже, /back"
const wrongNumber = "Ты ввел неправильное число!"
const fridgeUpdated = "Отлично, твой холодильник пополнен!"
const userNotFound = "Я не нашел тебя в нашем списке, попробуй для начала /start"
const needHelp = "Если тебе нужна помощь, то нажми /help"
const userBalance = balance + "\n\n Твой баланс: %d 💠\n\n Пополнение баланса: 🔜"
const mainMenu = "Добро пожаловать во FridgeFood! 🍽\nВыбери действие на панели меню."
const notEnoughBalance = "У тебя не хватает 💠! Пополни баланс."

const NotReady = "🛠 Функционал в разработке! 🛠"
