module gitlab.com/bonch.dev/hacks/hackuniversity-2020/eagles/fridgefood_bot

go 1.14

require (
	github.com/DATA-DOG/go-sqlmock v1.4.1 // indirect
	github.com/Prinzhorn/nicenshtein v0.0.0-20180416090056-3df51a9c0daf
	github.com/friendsofgo/errors v0.9.2 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/joho/godotenv v1.3.0
	github.com/kat-co/vala v0.0.0-20170210184112-42e1d8b61f12
	github.com/lithammer/fuzzysearch v1.1.0
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/viper v1.6.2
	github.com/volatiletech/inflect v0.0.0-20170731032912-e7201282ae8d // indirect
	github.com/volatiletech/null v8.0.0+incompatible
	github.com/volatiletech/sqlboiler v3.6.1+incompatible
	gitlab.com/bonch.dev/go-lib/telebot v0.0.0-20200320191502-7a7b0c5213a6
)
