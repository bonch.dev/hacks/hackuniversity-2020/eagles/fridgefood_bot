// Code generated by SQLBoiler 3.5.0 (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package models

import (
	"context"
	"database/sql"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"github.com/volatiletech/sqlboiler/queries/qmhelper"
	"github.com/volatiletech/sqlboiler/strmangle"
)

// ProductRecipe is an object representing the database table.
type ProductRecipe struct {
	RecipeID  int     `boil:"recipe_id" json:"recipe_id" toml:"recipe_id" yaml:"recipe_id"`
	ProductID int     `boil:"product_id" json:"product_id" toml:"product_id" yaml:"product_id"`
	Count     float32 `boil:"count" json:"count" toml:"count" yaml:"count"`

	R *productRecipeR `boil:"-" json:"-" toml:"-" yaml:"-"`
	L productRecipeL  `boil:"-" json:"-" toml:"-" yaml:"-"`
}

var ProductRecipeColumns = struct {
	RecipeID  string
	ProductID string
	Count     string
}{
	RecipeID:  "recipe_id",
	ProductID: "product_id",
	Count:     "count",
}

// Generated where

type whereHelperfloat32 struct{ field string }

func (w whereHelperfloat32) EQ(x float32) qm.QueryMod { return qmhelper.Where(w.field, qmhelper.EQ, x) }
func (w whereHelperfloat32) NEQ(x float32) qm.QueryMod {
	return qmhelper.Where(w.field, qmhelper.NEQ, x)
}
func (w whereHelperfloat32) LT(x float32) qm.QueryMod { return qmhelper.Where(w.field, qmhelper.LT, x) }
func (w whereHelperfloat32) LTE(x float32) qm.QueryMod {
	return qmhelper.Where(w.field, qmhelper.LTE, x)
}
func (w whereHelperfloat32) GT(x float32) qm.QueryMod { return qmhelper.Where(w.field, qmhelper.GT, x) }
func (w whereHelperfloat32) GTE(x float32) qm.QueryMod {
	return qmhelper.Where(w.field, qmhelper.GTE, x)
}

var ProductRecipeWhere = struct {
	RecipeID  whereHelperint
	ProductID whereHelperint
	Count     whereHelperfloat32
}{
	RecipeID:  whereHelperint{field: "`product_recipe`.`recipe_id`"},
	ProductID: whereHelperint{field: "`product_recipe`.`product_id`"},
	Count:     whereHelperfloat32{field: "`product_recipe`.`count`"},
}

// ProductRecipeRels is where relationship names are stored.
var ProductRecipeRels = struct {
	Recipe  string
	Product string
}{
	Recipe:  "Recipe",
	Product: "Product",
}

// productRecipeR is where relationships are stored.
type productRecipeR struct {
	Recipe  *Recipe
	Product *Product
}

// NewStruct creates a new relationship struct
func (*productRecipeR) NewStruct() *productRecipeR {
	return &productRecipeR{}
}

// productRecipeL is where Load methods for each relationship are stored.
type productRecipeL struct{}

var (
	productRecipeAllColumns            = []string{"recipe_id", "product_id", "count"}
	productRecipeColumnsWithoutDefault = []string{"recipe_id", "product_id", "count"}
	productRecipeColumnsWithDefault    = []string{}
	productRecipePrimaryKeyColumns     = []string{"recipe_id", "product_id"}
)

type (
	// ProductRecipeSlice is an alias for a slice of pointers to ProductRecipe.
	// This should generally be used opposed to []ProductRecipe.
	ProductRecipeSlice []*ProductRecipe
	// ProductRecipeHook is the signature for custom ProductRecipe hook methods
	ProductRecipeHook func(context.Context, boil.ContextExecutor, *ProductRecipe) error

	productRecipeQuery struct {
		*queries.Query
	}
)

// Cache for insert, update and upsert
var (
	productRecipeType                 = reflect.TypeOf(&ProductRecipe{})
	productRecipeMapping              = queries.MakeStructMapping(productRecipeType)
	productRecipePrimaryKeyMapping, _ = queries.BindMapping(productRecipeType, productRecipeMapping, productRecipePrimaryKeyColumns)
	productRecipeInsertCacheMut       sync.RWMutex
	productRecipeInsertCache          = make(map[string]insertCache)
	productRecipeUpdateCacheMut       sync.RWMutex
	productRecipeUpdateCache          = make(map[string]updateCache)
	productRecipeUpsertCacheMut       sync.RWMutex
	productRecipeUpsertCache          = make(map[string]insertCache)
)

var (
	// Force time package dependency for automated UpdatedAt/CreatedAt.
	_ = time.Second
	// Force qmhelper dependency for where clause generation (which doesn't
	// always happen)
	_ = qmhelper.Where
)

var productRecipeBeforeInsertHooks []ProductRecipeHook
var productRecipeBeforeUpdateHooks []ProductRecipeHook
var productRecipeBeforeDeleteHooks []ProductRecipeHook
var productRecipeBeforeUpsertHooks []ProductRecipeHook

var productRecipeAfterInsertHooks []ProductRecipeHook
var productRecipeAfterSelectHooks []ProductRecipeHook
var productRecipeAfterUpdateHooks []ProductRecipeHook
var productRecipeAfterDeleteHooks []ProductRecipeHook
var productRecipeAfterUpsertHooks []ProductRecipeHook

// doBeforeInsertHooks executes all "before insert" hooks.
func (o *ProductRecipe) doBeforeInsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range productRecipeBeforeInsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeUpdateHooks executes all "before Update" hooks.
func (o *ProductRecipe) doBeforeUpdateHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range productRecipeBeforeUpdateHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeDeleteHooks executes all "before Delete" hooks.
func (o *ProductRecipe) doBeforeDeleteHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range productRecipeBeforeDeleteHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeUpsertHooks executes all "before Upsert" hooks.
func (o *ProductRecipe) doBeforeUpsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range productRecipeBeforeUpsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterInsertHooks executes all "after Insert" hooks.
func (o *ProductRecipe) doAfterInsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range productRecipeAfterInsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterSelectHooks executes all "after Select" hooks.
func (o *ProductRecipe) doAfterSelectHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range productRecipeAfterSelectHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterUpdateHooks executes all "after Update" hooks.
func (o *ProductRecipe) doAfterUpdateHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range productRecipeAfterUpdateHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterDeleteHooks executes all "after Delete" hooks.
func (o *ProductRecipe) doAfterDeleteHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range productRecipeAfterDeleteHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterUpsertHooks executes all "after Upsert" hooks.
func (o *ProductRecipe) doAfterUpsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range productRecipeAfterUpsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// AddProductRecipeHook registers your hook function for all future operations.
func AddProductRecipeHook(hookPoint boil.HookPoint, productRecipeHook ProductRecipeHook) {
	switch hookPoint {
	case boil.BeforeInsertHook:
		productRecipeBeforeInsertHooks = append(productRecipeBeforeInsertHooks, productRecipeHook)
	case boil.BeforeUpdateHook:
		productRecipeBeforeUpdateHooks = append(productRecipeBeforeUpdateHooks, productRecipeHook)
	case boil.BeforeDeleteHook:
		productRecipeBeforeDeleteHooks = append(productRecipeBeforeDeleteHooks, productRecipeHook)
	case boil.BeforeUpsertHook:
		productRecipeBeforeUpsertHooks = append(productRecipeBeforeUpsertHooks, productRecipeHook)
	case boil.AfterInsertHook:
		productRecipeAfterInsertHooks = append(productRecipeAfterInsertHooks, productRecipeHook)
	case boil.AfterSelectHook:
		productRecipeAfterSelectHooks = append(productRecipeAfterSelectHooks, productRecipeHook)
	case boil.AfterUpdateHook:
		productRecipeAfterUpdateHooks = append(productRecipeAfterUpdateHooks, productRecipeHook)
	case boil.AfterDeleteHook:
		productRecipeAfterDeleteHooks = append(productRecipeAfterDeleteHooks, productRecipeHook)
	case boil.AfterUpsertHook:
		productRecipeAfterUpsertHooks = append(productRecipeAfterUpsertHooks, productRecipeHook)
	}
}

// One returns a single productRecipe record from the query.
func (q productRecipeQuery) One(ctx context.Context, exec boil.ContextExecutor) (*ProductRecipe, error) {
	o := &ProductRecipe{}

	queries.SetLimit(q.Query, 1)

	err := q.Bind(ctx, exec, o)
	if err != nil {
		if errors.Cause(err) == sql.ErrNoRows {
			return nil, sql.ErrNoRows
		}
		return nil, errors.Wrap(err, "models: failed to execute a one query for product_recipe")
	}

	if err := o.doAfterSelectHooks(ctx, exec); err != nil {
		return o, err
	}

	return o, nil
}

// All returns all ProductRecipe records from the query.
func (q productRecipeQuery) All(ctx context.Context, exec boil.ContextExecutor) (ProductRecipeSlice, error) {
	var o []*ProductRecipe

	err := q.Bind(ctx, exec, &o)
	if err != nil {
		return nil, errors.Wrap(err, "models: failed to assign all query results to ProductRecipe slice")
	}

	if len(productRecipeAfterSelectHooks) != 0 {
		for _, obj := range o {
			if err := obj.doAfterSelectHooks(ctx, exec); err != nil {
				return o, err
			}
		}
	}

	return o, nil
}

// Count returns the count of all ProductRecipe records in the query.
func (q productRecipeQuery) Count(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	var count int64

	queries.SetSelect(q.Query, nil)
	queries.SetCount(q.Query)

	err := q.Query.QueryRowContext(ctx, exec).Scan(&count)
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to count product_recipe rows")
	}

	return count, nil
}

// Exists checks if the row exists in the table.
func (q productRecipeQuery) Exists(ctx context.Context, exec boil.ContextExecutor) (bool, error) {
	var count int64

	queries.SetSelect(q.Query, nil)
	queries.SetCount(q.Query)
	queries.SetLimit(q.Query, 1)

	err := q.Query.QueryRowContext(ctx, exec).Scan(&count)
	if err != nil {
		return false, errors.Wrap(err, "models: failed to check if product_recipe exists")
	}

	return count > 0, nil
}

// Recipe pointed to by the foreign key.
func (o *ProductRecipe) Recipe(mods ...qm.QueryMod) recipeQuery {
	queryMods := []qm.QueryMod{
		qm.Where("`id` = ?", o.RecipeID),
	}

	queryMods = append(queryMods, mods...)

	query := Recipes(queryMods...)
	queries.SetFrom(query.Query, "`recipes`")

	return query
}

// Product pointed to by the foreign key.
func (o *ProductRecipe) Product(mods ...qm.QueryMod) productQuery {
	queryMods := []qm.QueryMod{
		qm.Where("`id` = ?", o.ProductID),
	}

	queryMods = append(queryMods, mods...)

	query := Products(queryMods...)
	queries.SetFrom(query.Query, "`products`")

	return query
}

// LoadRecipe allows an eager lookup of values, cached into the
// loaded structs of the objects. This is for an N-1 relationship.
func (productRecipeL) LoadRecipe(ctx context.Context, e boil.ContextExecutor, singular bool, maybeProductRecipe interface{}, mods queries.Applicator) error {
	var slice []*ProductRecipe
	var object *ProductRecipe

	if singular {
		object = maybeProductRecipe.(*ProductRecipe)
	} else {
		slice = *maybeProductRecipe.(*[]*ProductRecipe)
	}

	args := make([]interface{}, 0, 1)
	if singular {
		if object.R == nil {
			object.R = &productRecipeR{}
		}
		args = append(args, object.RecipeID)

	} else {
	Outer:
		for _, obj := range slice {
			if obj.R == nil {
				obj.R = &productRecipeR{}
			}

			for _, a := range args {
				if a == obj.RecipeID {
					continue Outer
				}
			}

			args = append(args, obj.RecipeID)

		}
	}

	if len(args) == 0 {
		return nil
	}

	query := NewQuery(qm.From(`recipes`), qm.WhereIn(`recipes.id in ?`, args...))
	if mods != nil {
		mods.Apply(query)
	}

	results, err := query.QueryContext(ctx, e)
	if err != nil {
		return errors.Wrap(err, "failed to eager load Recipe")
	}

	var resultSlice []*Recipe
	if err = queries.Bind(results, &resultSlice); err != nil {
		return errors.Wrap(err, "failed to bind eager loaded slice Recipe")
	}

	if err = results.Close(); err != nil {
		return errors.Wrap(err, "failed to close results of eager load for recipes")
	}
	if err = results.Err(); err != nil {
		return errors.Wrap(err, "error occurred during iteration of eager loaded relations for recipes")
	}

	if len(productRecipeAfterSelectHooks) != 0 {
		for _, obj := range resultSlice {
			if err := obj.doAfterSelectHooks(ctx, e); err != nil {
				return err
			}
		}
	}

	if len(resultSlice) == 0 {
		return nil
	}

	if singular {
		foreign := resultSlice[0]
		object.R.Recipe = foreign
		if foreign.R == nil {
			foreign.R = &recipeR{}
		}
		foreign.R.ProductRecipes = append(foreign.R.ProductRecipes, object)
		return nil
	}

	for _, local := range slice {
		for _, foreign := range resultSlice {
			if local.RecipeID == foreign.ID {
				local.R.Recipe = foreign
				if foreign.R == nil {
					foreign.R = &recipeR{}
				}
				foreign.R.ProductRecipes = append(foreign.R.ProductRecipes, local)
				break
			}
		}
	}

	return nil
}

// LoadProduct allows an eager lookup of values, cached into the
// loaded structs of the objects. This is for an N-1 relationship.
func (productRecipeL) LoadProduct(ctx context.Context, e boil.ContextExecutor, singular bool, maybeProductRecipe interface{}, mods queries.Applicator) error {
	var slice []*ProductRecipe
	var object *ProductRecipe

	if singular {
		object = maybeProductRecipe.(*ProductRecipe)
	} else {
		slice = *maybeProductRecipe.(*[]*ProductRecipe)
	}

	args := make([]interface{}, 0, 1)
	if singular {
		if object.R == nil {
			object.R = &productRecipeR{}
		}
		args = append(args, object.ProductID)

	} else {
	Outer:
		for _, obj := range slice {
			if obj.R == nil {
				obj.R = &productRecipeR{}
			}

			for _, a := range args {
				if a == obj.ProductID {
					continue Outer
				}
			}

			args = append(args, obj.ProductID)

		}
	}

	if len(args) == 0 {
		return nil
	}

	query := NewQuery(qm.From(`products`), qm.WhereIn(`products.id in ?`, args...))
	if mods != nil {
		mods.Apply(query)
	}

	results, err := query.QueryContext(ctx, e)
	if err != nil {
		return errors.Wrap(err, "failed to eager load Product")
	}

	var resultSlice []*Product
	if err = queries.Bind(results, &resultSlice); err != nil {
		return errors.Wrap(err, "failed to bind eager loaded slice Product")
	}

	if err = results.Close(); err != nil {
		return errors.Wrap(err, "failed to close results of eager load for products")
	}
	if err = results.Err(); err != nil {
		return errors.Wrap(err, "error occurred during iteration of eager loaded relations for products")
	}

	if len(productRecipeAfterSelectHooks) != 0 {
		for _, obj := range resultSlice {
			if err := obj.doAfterSelectHooks(ctx, e); err != nil {
				return err
			}
		}
	}

	if len(resultSlice) == 0 {
		return nil
	}

	if singular {
		foreign := resultSlice[0]
		object.R.Product = foreign
		if foreign.R == nil {
			foreign.R = &productR{}
		}
		foreign.R.ProductRecipes = append(foreign.R.ProductRecipes, object)
		return nil
	}

	for _, local := range slice {
		for _, foreign := range resultSlice {
			if local.ProductID == foreign.ID {
				local.R.Product = foreign
				if foreign.R == nil {
					foreign.R = &productR{}
				}
				foreign.R.ProductRecipes = append(foreign.R.ProductRecipes, local)
				break
			}
		}
	}

	return nil
}

// SetRecipe of the productRecipe to the related item.
// Sets o.R.Recipe to related.
// Adds o to related.R.ProductRecipes.
func (o *ProductRecipe) SetRecipe(ctx context.Context, exec boil.ContextExecutor, insert bool, related *Recipe) error {
	var err error
	if insert {
		if err = related.Insert(ctx, exec, boil.Infer()); err != nil {
			return errors.Wrap(err, "failed to insert into foreign table")
		}
	}

	updateQuery := fmt.Sprintf(
		"UPDATE `product_recipe` SET %s WHERE %s",
		strmangle.SetParamNames("`", "`", 0, []string{"recipe_id"}),
		strmangle.WhereClause("`", "`", 0, productRecipePrimaryKeyColumns),
	)
	values := []interface{}{related.ID, o.RecipeID, o.ProductID}

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, updateQuery)
		fmt.Fprintln(boil.DebugWriter, values)
	}

	if _, err = exec.ExecContext(ctx, updateQuery, values...); err != nil {
		return errors.Wrap(err, "failed to update local table")
	}

	o.RecipeID = related.ID
	if o.R == nil {
		o.R = &productRecipeR{
			Recipe: related,
		}
	} else {
		o.R.Recipe = related
	}

	if related.R == nil {
		related.R = &recipeR{
			ProductRecipes: ProductRecipeSlice{o},
		}
	} else {
		related.R.ProductRecipes = append(related.R.ProductRecipes, o)
	}

	return nil
}

// SetProduct of the productRecipe to the related item.
// Sets o.R.Product to related.
// Adds o to related.R.ProductRecipes.
func (o *ProductRecipe) SetProduct(ctx context.Context, exec boil.ContextExecutor, insert bool, related *Product) error {
	var err error
	if insert {
		if err = related.Insert(ctx, exec, boil.Infer()); err != nil {
			return errors.Wrap(err, "failed to insert into foreign table")
		}
	}

	updateQuery := fmt.Sprintf(
		"UPDATE `product_recipe` SET %s WHERE %s",
		strmangle.SetParamNames("`", "`", 0, []string{"product_id"}),
		strmangle.WhereClause("`", "`", 0, productRecipePrimaryKeyColumns),
	)
	values := []interface{}{related.ID, o.RecipeID, o.ProductID}

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, updateQuery)
		fmt.Fprintln(boil.DebugWriter, values)
	}

	if _, err = exec.ExecContext(ctx, updateQuery, values...); err != nil {
		return errors.Wrap(err, "failed to update local table")
	}

	o.ProductID = related.ID
	if o.R == nil {
		o.R = &productRecipeR{
			Product: related,
		}
	} else {
		o.R.Product = related
	}

	if related.R == nil {
		related.R = &productR{
			ProductRecipes: ProductRecipeSlice{o},
		}
	} else {
		related.R.ProductRecipes = append(related.R.ProductRecipes, o)
	}

	return nil
}

// ProductRecipes retrieves all the records using an executor.
func ProductRecipes(mods ...qm.QueryMod) productRecipeQuery {
	mods = append(mods, qm.From("`product_recipe`"))
	return productRecipeQuery{NewQuery(mods...)}
}

// FindProductRecipe retrieves a single record by ID with an executor.
// If selectCols is empty Find will return all columns.
func FindProductRecipe(ctx context.Context, exec boil.ContextExecutor, recipeID int, productID int, selectCols ...string) (*ProductRecipe, error) {
	productRecipeObj := &ProductRecipe{}

	sel := "*"
	if len(selectCols) > 0 {
		sel = strings.Join(strmangle.IdentQuoteSlice(dialect.LQ, dialect.RQ, selectCols), ",")
	}
	query := fmt.Sprintf(
		"select %s from `product_recipe` where `recipe_id`=? AND `product_id`=?", sel,
	)

	q := queries.Raw(query, recipeID, productID)

	err := q.Bind(ctx, exec, productRecipeObj)
	if err != nil {
		if errors.Cause(err) == sql.ErrNoRows {
			return nil, sql.ErrNoRows
		}
		return nil, errors.Wrap(err, "models: unable to select from product_recipe")
	}

	return productRecipeObj, nil
}

// Insert a single record using an executor.
// See boil.Columns.InsertColumnSet documentation to understand column list inference for inserts.
func (o *ProductRecipe) Insert(ctx context.Context, exec boil.ContextExecutor, columns boil.Columns) error {
	if o == nil {
		return errors.New("models: no product_recipe provided for insertion")
	}

	var err error

	if err := o.doBeforeInsertHooks(ctx, exec); err != nil {
		return err
	}

	nzDefaults := queries.NonZeroDefaultSet(productRecipeColumnsWithDefault, o)

	key := makeCacheKey(columns, nzDefaults)
	productRecipeInsertCacheMut.RLock()
	cache, cached := productRecipeInsertCache[key]
	productRecipeInsertCacheMut.RUnlock()

	if !cached {
		wl, returnColumns := columns.InsertColumnSet(
			productRecipeAllColumns,
			productRecipeColumnsWithDefault,
			productRecipeColumnsWithoutDefault,
			nzDefaults,
		)

		cache.valueMapping, err = queries.BindMapping(productRecipeType, productRecipeMapping, wl)
		if err != nil {
			return err
		}
		cache.retMapping, err = queries.BindMapping(productRecipeType, productRecipeMapping, returnColumns)
		if err != nil {
			return err
		}
		if len(wl) != 0 {
			cache.query = fmt.Sprintf("INSERT INTO `product_recipe` (`%s`) %%sVALUES (%s)%%s", strings.Join(wl, "`,`"), strmangle.Placeholders(dialect.UseIndexPlaceholders, len(wl), 1, 1))
		} else {
			cache.query = "INSERT INTO `product_recipe` () VALUES ()%s%s"
		}

		var queryOutput, queryReturning string

		if len(cache.retMapping) != 0 {
			cache.retQuery = fmt.Sprintf("SELECT `%s` FROM `product_recipe` WHERE %s", strings.Join(returnColumns, "`,`"), strmangle.WhereClause("`", "`", 0, productRecipePrimaryKeyColumns))
		}

		cache.query = fmt.Sprintf(cache.query, queryOutput, queryReturning)
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	vals := queries.ValuesFromMapping(value, cache.valueMapping)

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, cache.query)
		fmt.Fprintln(boil.DebugWriter, vals)
	}

	_, err = exec.ExecContext(ctx, cache.query, vals...)

	if err != nil {
		return errors.Wrap(err, "models: unable to insert into product_recipe")
	}

	var identifierCols []interface{}

	if len(cache.retMapping) == 0 {
		goto CacheNoHooks
	}

	identifierCols = []interface{}{
		o.RecipeID,
		o.ProductID,
	}

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, cache.retQuery)
		fmt.Fprintln(boil.DebugWriter, identifierCols...)
	}

	err = exec.QueryRowContext(ctx, cache.retQuery, identifierCols...).Scan(queries.PtrsFromMapping(value, cache.retMapping)...)
	if err != nil {
		return errors.Wrap(err, "models: unable to populate default values for product_recipe")
	}

CacheNoHooks:
	if !cached {
		productRecipeInsertCacheMut.Lock()
		productRecipeInsertCache[key] = cache
		productRecipeInsertCacheMut.Unlock()
	}

	return o.doAfterInsertHooks(ctx, exec)
}

// Update uses an executor to update the ProductRecipe.
// See boil.Columns.UpdateColumnSet documentation to understand column list inference for updates.
// Update does not automatically update the record in case of default values. Use .Reload() to refresh the records.
func (o *ProductRecipe) Update(ctx context.Context, exec boil.ContextExecutor, columns boil.Columns) (int64, error) {
	var err error
	if err = o.doBeforeUpdateHooks(ctx, exec); err != nil {
		return 0, err
	}
	key := makeCacheKey(columns, nil)
	productRecipeUpdateCacheMut.RLock()
	cache, cached := productRecipeUpdateCache[key]
	productRecipeUpdateCacheMut.RUnlock()

	if !cached {
		wl := columns.UpdateColumnSet(
			productRecipeAllColumns,
			productRecipePrimaryKeyColumns,
		)

		if !columns.IsWhitelist() {
			wl = strmangle.SetComplement(wl, []string{"created_at"})
		}
		if len(wl) == 0 {
			return 0, errors.New("models: unable to update product_recipe, could not build whitelist")
		}

		cache.query = fmt.Sprintf("UPDATE `product_recipe` SET %s WHERE %s",
			strmangle.SetParamNames("`", "`", 0, wl),
			strmangle.WhereClause("`", "`", 0, productRecipePrimaryKeyColumns),
		)
		cache.valueMapping, err = queries.BindMapping(productRecipeType, productRecipeMapping, append(wl, productRecipePrimaryKeyColumns...))
		if err != nil {
			return 0, err
		}
	}

	values := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(o)), cache.valueMapping)

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, cache.query)
		fmt.Fprintln(boil.DebugWriter, values)
	}

	var result sql.Result
	result, err = exec.ExecContext(ctx, cache.query, values...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update product_recipe row")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by update for product_recipe")
	}

	if !cached {
		productRecipeUpdateCacheMut.Lock()
		productRecipeUpdateCache[key] = cache
		productRecipeUpdateCacheMut.Unlock()
	}

	return rowsAff, o.doAfterUpdateHooks(ctx, exec)
}

// UpdateAll updates all rows with the specified column values.
func (q productRecipeQuery) UpdateAll(ctx context.Context, exec boil.ContextExecutor, cols M) (int64, error) {
	queries.SetUpdate(q.Query, cols)

	result, err := q.Query.ExecContext(ctx, exec)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update all for product_recipe")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to retrieve rows affected for product_recipe")
	}

	return rowsAff, nil
}

// UpdateAll updates all rows with the specified column values, using an executor.
func (o ProductRecipeSlice) UpdateAll(ctx context.Context, exec boil.ContextExecutor, cols M) (int64, error) {
	ln := int64(len(o))
	if ln == 0 {
		return 0, nil
	}

	if len(cols) == 0 {
		return 0, errors.New("models: update all requires at least one column argument")
	}

	colNames := make([]string, len(cols))
	args := make([]interface{}, len(cols))

	i := 0
	for name, value := range cols {
		colNames[i] = name
		args[i] = value
		i++
	}

	// Append all of the primary key values for each column
	for _, obj := range o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), productRecipePrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := fmt.Sprintf("UPDATE `product_recipe` SET %s WHERE %s",
		strmangle.SetParamNames("`", "`", 0, colNames),
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), 0, productRecipePrimaryKeyColumns, len(o)))

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, sql)
		fmt.Fprintln(boil.DebugWriter, args...)
	}

	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update all in productRecipe slice")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to retrieve rows affected all in update all productRecipe")
	}
	return rowsAff, nil
}

var mySQLProductRecipeUniqueColumns = []string{}

// Upsert attempts an insert using an executor, and does an update or ignore on conflict.
// See boil.Columns documentation for how to properly use updateColumns and insertColumns.
func (o *ProductRecipe) Upsert(ctx context.Context, exec boil.ContextExecutor, updateColumns, insertColumns boil.Columns) error {
	if o == nil {
		return errors.New("models: no product_recipe provided for upsert")
	}

	if err := o.doBeforeUpsertHooks(ctx, exec); err != nil {
		return err
	}

	nzDefaults := queries.NonZeroDefaultSet(productRecipeColumnsWithDefault, o)
	nzUniques := queries.NonZeroDefaultSet(mySQLProductRecipeUniqueColumns, o)

	if len(nzUniques) == 0 {
		return errors.New("cannot upsert with a table that cannot conflict on a unique column")
	}

	// Build cache key in-line uglily - mysql vs psql problems
	buf := strmangle.GetBuffer()
	buf.WriteString(strconv.Itoa(updateColumns.Kind))
	for _, c := range updateColumns.Cols {
		buf.WriteString(c)
	}
	buf.WriteByte('.')
	buf.WriteString(strconv.Itoa(insertColumns.Kind))
	for _, c := range insertColumns.Cols {
		buf.WriteString(c)
	}
	buf.WriteByte('.')
	for _, c := range nzDefaults {
		buf.WriteString(c)
	}
	buf.WriteByte('.')
	for _, c := range nzUniques {
		buf.WriteString(c)
	}
	key := buf.String()
	strmangle.PutBuffer(buf)

	productRecipeUpsertCacheMut.RLock()
	cache, cached := productRecipeUpsertCache[key]
	productRecipeUpsertCacheMut.RUnlock()

	var err error

	if !cached {
		insert, ret := insertColumns.InsertColumnSet(
			productRecipeAllColumns,
			productRecipeColumnsWithDefault,
			productRecipeColumnsWithoutDefault,
			nzDefaults,
		)
		update := updateColumns.UpdateColumnSet(
			productRecipeAllColumns,
			productRecipePrimaryKeyColumns,
		)

		if len(update) == 0 {
			return errors.New("models: unable to upsert product_recipe, could not build update column list")
		}

		ret = strmangle.SetComplement(ret, nzUniques)
		cache.query = buildUpsertQueryMySQL(dialect, "product_recipe", update, insert)
		cache.retQuery = fmt.Sprintf(
			"SELECT %s FROM `product_recipe` WHERE %s",
			strings.Join(strmangle.IdentQuoteSlice(dialect.LQ, dialect.RQ, ret), ","),
			strmangle.WhereClause("`", "`", 0, nzUniques),
		)

		cache.valueMapping, err = queries.BindMapping(productRecipeType, productRecipeMapping, insert)
		if err != nil {
			return err
		}
		if len(ret) != 0 {
			cache.retMapping, err = queries.BindMapping(productRecipeType, productRecipeMapping, ret)
			if err != nil {
				return err
			}
		}
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	vals := queries.ValuesFromMapping(value, cache.valueMapping)
	var returns []interface{}
	if len(cache.retMapping) != 0 {
		returns = queries.PtrsFromMapping(value, cache.retMapping)
	}

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, cache.query)
		fmt.Fprintln(boil.DebugWriter, vals)
	}

	_, err = exec.ExecContext(ctx, cache.query, vals...)

	if err != nil {
		return errors.Wrap(err, "models: unable to upsert for product_recipe")
	}

	var uniqueMap []uint64
	var nzUniqueCols []interface{}

	if len(cache.retMapping) == 0 {
		goto CacheNoHooks
	}

	uniqueMap, err = queries.BindMapping(productRecipeType, productRecipeMapping, nzUniques)
	if err != nil {
		return errors.Wrap(err, "models: unable to retrieve unique values for product_recipe")
	}
	nzUniqueCols = queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(o)), uniqueMap)

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, cache.retQuery)
		fmt.Fprintln(boil.DebugWriter, nzUniqueCols...)
	}

	err = exec.QueryRowContext(ctx, cache.retQuery, nzUniqueCols...).Scan(returns...)
	if err != nil {
		return errors.Wrap(err, "models: unable to populate default values for product_recipe")
	}

CacheNoHooks:
	if !cached {
		productRecipeUpsertCacheMut.Lock()
		productRecipeUpsertCache[key] = cache
		productRecipeUpsertCacheMut.Unlock()
	}

	return o.doAfterUpsertHooks(ctx, exec)
}

// Delete deletes a single ProductRecipe record with an executor.
// Delete will match against the primary key column to find the record to delete.
func (o *ProductRecipe) Delete(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if o == nil {
		return 0, errors.New("models: no ProductRecipe provided for delete")
	}

	if err := o.doBeforeDeleteHooks(ctx, exec); err != nil {
		return 0, err
	}

	args := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(o)), productRecipePrimaryKeyMapping)
	sql := "DELETE FROM `product_recipe` WHERE `recipe_id`=? AND `product_id`=?"

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, sql)
		fmt.Fprintln(boil.DebugWriter, args...)
	}

	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete from product_recipe")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by delete for product_recipe")
	}

	if err := o.doAfterDeleteHooks(ctx, exec); err != nil {
		return 0, err
	}

	return rowsAff, nil
}

// DeleteAll deletes all matching rows.
func (q productRecipeQuery) DeleteAll(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if q.Query == nil {
		return 0, errors.New("models: no productRecipeQuery provided for delete all")
	}

	queries.SetDelete(q.Query)

	result, err := q.Query.ExecContext(ctx, exec)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete all from product_recipe")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by deleteall for product_recipe")
	}

	return rowsAff, nil
}

// DeleteAll deletes all rows in the slice, using an executor.
func (o ProductRecipeSlice) DeleteAll(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if len(o) == 0 {
		return 0, nil
	}

	if len(productRecipeBeforeDeleteHooks) != 0 {
		for _, obj := range o {
			if err := obj.doBeforeDeleteHooks(ctx, exec); err != nil {
				return 0, err
			}
		}
	}

	var args []interface{}
	for _, obj := range o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), productRecipePrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := "DELETE FROM `product_recipe` WHERE " +
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), 0, productRecipePrimaryKeyColumns, len(o))

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, sql)
		fmt.Fprintln(boil.DebugWriter, args)
	}

	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete all from productRecipe slice")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by deleteall for product_recipe")
	}

	if len(productRecipeAfterDeleteHooks) != 0 {
		for _, obj := range o {
			if err := obj.doAfterDeleteHooks(ctx, exec); err != nil {
				return 0, err
			}
		}
	}

	return rowsAff, nil
}

// Reload refetches the object from the database
// using the primary keys with an executor.
func (o *ProductRecipe) Reload(ctx context.Context, exec boil.ContextExecutor) error {
	ret, err := FindProductRecipe(ctx, exec, o.RecipeID, o.ProductID)
	if err != nil {
		return err
	}

	*o = *ret
	return nil
}

// ReloadAll refetches every row with matching primary key column values
// and overwrites the original object slice with the newly updated slice.
func (o *ProductRecipeSlice) ReloadAll(ctx context.Context, exec boil.ContextExecutor) error {
	if o == nil || len(*o) == 0 {
		return nil
	}

	slice := ProductRecipeSlice{}
	var args []interface{}
	for _, obj := range *o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), productRecipePrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := "SELECT `product_recipe`.* FROM `product_recipe` WHERE " +
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), 0, productRecipePrimaryKeyColumns, len(*o))

	q := queries.Raw(sql, args...)

	err := q.Bind(ctx, exec, &slice)
	if err != nil {
		return errors.Wrap(err, "models: unable to reload all in ProductRecipeSlice")
	}

	*o = slice

	return nil
}

// ProductRecipeExists checks if the ProductRecipe row exists.
func ProductRecipeExists(ctx context.Context, exec boil.ContextExecutor, recipeID int, productID int) (bool, error) {
	var exists bool
	sql := "select exists(select 1 from `product_recipe` where `recipe_id`=? AND `product_id`=? limit 1)"

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, sql)
		fmt.Fprintln(boil.DebugWriter, recipeID, productID)
	}

	row := exec.QueryRowContext(ctx, sql, recipeID, productID)

	err := row.Scan(&exists)
	if err != nil {
		return false, errors.Wrap(err, "models: unable to check if product_recipe exists")
	}

	return exists, nil
}
